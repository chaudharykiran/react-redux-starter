import imageRegXl from "../../images/png/image-reg-xl-min.png";
import imageRegLg from "../../images/png/image-reg-lg-min.png";
import imageRegMd from "../../images/png/image-reg-md-min.png";
import imageRegSm from "../../images/png/image-reg-sm-min.png";

export default ({ breakpoints }) => ({
  wrapper: {
    width: "100%",
    paddingRight: 20,
    paddingLeft: 20,

    [breakpoints.up("md")]: {
      maxWidth: 700,
      margin: "0 auto"
    }
  },

  regBackgroundWrapper: {
    display: "block",
    height: "calc(100% + 100vh)",
    backgroundImage: `url('${imageRegSm}')`,
    backgroundRepeat: "no-repeat",
    backgroundAttachment: "fixed",
    backgroundSize: "cover",

    [breakpoints.up("md")]: {
      backgroundImage: `url('${imageRegMd}')`
    },

    [breakpoints.up("lg")]: {
      backgroundImage: `url('${imageRegLg}')`
    },

    [breakpoints.up("xl")]: {
      backgroundImage: `url('${imageRegXl}')`
    }
  }
});
