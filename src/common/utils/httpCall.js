import axios from "axios";
// import AppEnv from "./config/app.local";

const PORT = "3000";
export const BASE_URI = `http://localhost:${PORT}`;
// export const BASE_URI = `http://128.199.176.34:${PORT}`;
// let exportURL;
// if (process.env.environment == 'Production') { exportURL = `http://104.248.191.48:${PORT}`; } else if (process.env.environment == 'Development') { exportURL = `http://128.199.176.34:${PORT}`; }

// export const BASE_URI = exportURL;
export const API_BASE_URI = `${BASE_URI}/api`;

export const API = {
  serverCall: async (
    uri,
    method = "GET",
    data = {},
    options = {
      withResponseHeader: false
    }
  ) => {
    const query = {
      method,
      url: `${API_BASE_URI}${uri}`.trim(),
      data
    };

    if (method === "POST" || method === "post") {
      query.data = data;
    }

    // if (headers) {
    //   query.headers = headers;
    // }

    const response = await axios(query);
    if (options.withResponseHeader) {
      return {
        headers: {
          ...response.headers
        },
        ...response.data
      };
    }

    return response.data;
  }
};

export default {
  GET: (url, data, options) => {
    return API.serverCall(url, "GET", data, options);
  },

  POST: (url, data, options) => {
    return API.serverCall(url, "POST", data, options);
  },

  PUT: (url, data, options) => {
    return API.serverCall(url, "PUT", data, options);
  },

  PATCH: (url, data, options) => {
    return API.serverCall(url, "PATCH", data, options);
  },

  DELETE: (url, data, options) => {
    return API.serverCall(url, "DELETE", data, options);
  }
};
