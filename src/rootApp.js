import React from "react";
// import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import WrapThemeProvider from "./common/components/wrapThemeProvider";
import setUpStore from "./store/configureStore";

import Register from "./modules/Register/Register";

const store = setUpStore();

function AppWrapper(props) {
  return (
    <Provider store={store}>
      <Router store={store}>
        <WrapThemeProvider>
          <Register />
        </WrapThemeProvider>
      </Router>
    </Provider>
  );
}

export default AppWrapper;
