import {configureStore, getDefaultMiddleware} from 'redux-starter-kit'
import logger from 'redux-logger'

import rootReducer from '../reducers'

const middleware = [...getDefaultMiddleware(), logger]

const setUpStore = (preloadedState) => {
  const store = configureStore({
    reducer: rootReducer,
    middleware,
    devTools: process.env.NODE_ENV !== 'production',
    preloadedState
  })

  return store
}

export default setUpStore